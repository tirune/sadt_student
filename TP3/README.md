## sensorCAN

Pour réaliser le sensorCAN.c on réutilise le code de cantransmit auquel on ajoute deux fonctions principales : 
 - receive (reception des données)
 - send (émission des données)
 - concatenate (adaptation des données au format voulu)

### receive_sensor_data

receive_sensor_data fonctionne comme canreceive et va adapter les données reçu pour l'emission.

```python
while (1) {
    // Receive sensor frames
    if (read(s_receive, &frame, sizeof(struct canfd_frame)) < 0) {
        perror("Receive Read");
    }

    // Concatenate sensor data
    concatenate_sensor_data(&frame, concatenated_data);

    // Check if all sensor data is collected
    int all_data_received = 1;
    for (int i = 0; i < SENSOR_COUNT * SENSOR_DATA_LENGTH; i++) {
        if (concatenated_data[i] == 0) {
            all_data_received = 0;
            break;
        }
    }

    // If all data received, print concatenated data
    if (all_data_received) {
        printf("Received concatenated data: ");
        for (int i = 0; i < SENSOR_COUNT * SENSOR_DATA_LENGTH; i++) {
            printf("%02X ", concatenated_data[i]);
        }
        printf("\n");

        // Reset concatenated data
        memset(concatenated_data, 0, sizeof(concatenated_data));
    }
}

```

### send_concatenated_data

send_concatenated_data fonctionne comme cantransmit mais va organiser nos données pour correspondre au requétes de l'énoncé.

```python
while (1) {
    // Example concatenated data
    unsigned char concatenated_data[SENSOR_COUNT * SENSOR_DATA_LENGTH] = {
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
        0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
        0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20,
        0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
        0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30
    };

    // Create CAN FD frame
    struct canfd_frame fd_frame;
    memset(&fd_frame, 0, sizeof(fd_frame));
    fd_frame.can_id = 0x100;
    fd_frame.len = SENSOR_COUNT * SENSOR_DATA_LENGTH;
    memcpy(fd_frame.data, concatenated_data, SENSOR_COUNT * SENSOR_DATA_LENGTH);

    // Send CAN FD frame
    if (write(s_send, &fd_frame, sizeof(struct canfd_frame)) != sizeof(struct canfd_frame)) {
        perror("Send Write");
    }

    // Wait before sending again (example delay)
    usleep(1000000); // 1 second
}

```

### Résultat

Le code n'était pas fini en cours j'avais des problémes de rendu au niveau du send, je l'ai améliorer chez moi mais je n'ai pas pu tester le résultat à cause d'un probléme d'OS compatibility avec WSL.