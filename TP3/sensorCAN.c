#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#define SENSOR_COUNT 6
#define SENSOR_DATA_LENGTH 8

// Function to concatenate sensor data
void concatenate_sensor_data(struct canfd_frame *frame, unsigned char *concatenated_data) {
    int sensor_index = frame->can_id - 0xC00;
    int start_byte = sensor_index * SENSOR_DATA_LENGTH;
    memcpy(concatenated_data + start_byte, frame->data, SENSOR_DATA_LENGTH);
}

// Function to receive sensor data
void receive_sensor_data() {
    int s_receive;
    struct sockaddr_can addr_receive;
    struct ifreq ifr_receive;
    struct canfd_frame frame;
    unsigned char concatenated_data[SENSOR_COUNT * SENSOR_DATA_LENGTH] = {0};

    // Create receive socket
    if ((s_receive = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Receive Socket");
    }

    // Specify receive interface
    (void)strcpy(ifr_receive.ifr_name, "vcan0");
    ioctl(s_receive, SIOCGIFINDEX, &ifr_receive);

    // Initialize receive address structure
    (void)memset(&addr_receive, 0, sizeof(addr_receive));
    addr_receive.can_family = AF_CAN;
    addr_receive.can_ifindex = ifr_receive.ifr_ifindex;

    // Bind receive socket to the specified interface
    if (bind(s_receive, (struct sockaddr *)&addr_receive, sizeof(addr_receive)) < 0) {
        perror("Receive Bind");
    }

    // Main loop to receive and concatenate sensor data
    while (1) {
        // Receive sensor frames
        if (read(s_receive, &frame, sizeof(struct canfd_frame)) < 0) {
            perror("Receive Read");
        }

        // Concatenate sensor data
        concatenate_sensor_data(&frame, concatenated_data);

        // Check if all sensor data is collected
        int all_data_received = 1;
        for (int i = 0; i < SENSOR_COUNT * SENSOR_DATA_LENGTH; i++) {
            if (concatenated_data[i] == 0) {
                all_data_received = 0;
                break;
            }
        }

        // If all data received, print concatenated data
        if (all_data_received) {
            printf("Received concatenated data: ");
            for (int i = 0; i < SENSOR_COUNT * SENSOR_DATA_LENGTH; i++) {
                printf("%02X ", concatenated_data[i]);
            }
            printf("\n");

            // Reset concatenated data
            memset(concatenated_data, 0, sizeof(concatenated_data));
        }
    }

    // Close receive socket
    close(s_receive);
}

// Function to send concatenated data
void send_concatenated_data() {
    int s_send;
    struct sockaddr_can addr_send;
    struct ifreq ifr_send;

    // Create send socket
    if ((s_send = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Send Socket");
    }

    // Specify send interface
    (void)strcpy(ifr_send.ifr_name, "vcan2");
    ioctl(s_send, SIOCGIFINDEX, &ifr_send);

    // Initialize send address structure
    (void)memset(&addr_send, 0, sizeof(addr_send));
    addr_send.can_family = AF_CAN;
    addr_send.can_ifindex = ifr_send.ifr_ifindex;

    // Enable CAN FD frames
    int enable_canfd = 1;
    if (setsockopt(s_send, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &enable_canfd, sizeof(enable_canfd)) < 0) {
        perror("Setsockopt");
    }

    // Main loop to send concatenated data
    while (1) {
        // Example concatenated data
        unsigned char concatenated_data[SENSOR_COUNT * SENSOR_DATA_LENGTH] = {
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
            0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
            0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
            0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20,
            0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
            0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30
        };

        // Create CAN FD frame
        struct canfd_frame fd_frame;
        memset(&fd_frame, 0, sizeof(fd_frame));
        fd_frame.can_id = 0x100;
        fd_frame.len = SENSOR_COUNT * SENSOR_DATA_LENGTH;
        memcpy(fd_frame.data, concatenated_data, SENSOR_COUNT * SENSOR_DATA_LENGTH);

        // Send CAN FD frame
        if (write(s_send, &fd_frame, sizeof(struct canfd_frame)) != sizeof(struct canfd_frame)) {
            perror("Send Write");
        }

        // Wait before sending again (example delay)
        usleep(1000000); // 1 second
    }

    // Close send socket
    close(s_send);
}

int main(void) {    // Run receive sensor data function in a separate thread
    if (fork() == 0) {
        receive_sensor_data();
    }

    // Run send concatenated data function in the main thread
    send_concatenated_data();

    return 0;
}
       
