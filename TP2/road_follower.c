#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
// Fonction reset
void resetlight(int s) {
    struct can_frame frame;
    frame.can_id = 0x123;
    frame.can_dlc = 2;
    frame.data[0] = 0x00;
    frame.data[1] = 0x00;
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}

void blink_right(int s) {
    struct can_frame frame;
    frame.can_id = 0x123;
    frame.can_dlc = 2;
    frame.data[0] = 0x00;
    frame.data[1] = 0x01; 
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}

void blink_left(int s) {
    struct can_frame frame;
    frame.can_id = 0x123;
    frame.can_dlc = 2;
    frame.data[0] = 0x00;
    frame.data[1] = 0x02;
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}

void low_beam(int s) {
    struct can_frame frame;
    frame.can_id = 0x123;
    frame.can_dlc = 2;
    frame.data[0] = 0x01;
    frame.data[1] = 0x00;
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}
void high_beam(int s) {
    struct can_frame frame;
    frame.can_id = 0x123;
    frame.can_dlc = 2;
    frame.data[0] = 0x02;
    frame.data[1] = 0x00;
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}
// Fonction pour accélerer
void accelerate(int s) {
    struct can_frame frame;
    frame.can_id = 0x321;
    frame.can_dlc = 3;
    frame.data[0] = 100; // Throttle
    frame.data[1] = 0;  // Brake
    frame.data[2] = 0;  // Steering
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}

// Fonction pour freiner
void brake(int s) {
    struct can_frame frame;
    frame.can_id = 0x321;
    frame.can_dlc = 3;
    frame.data[0] = 0; // Throttle
    frame.data[1] = 100;  // Brake
    frame.data[2] = 0;  // Steering
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}


// Fonction pour tourner à droite
void turn_right(int s) {
    struct can_frame frame;
    frame.can_id = 0x321;
    frame.can_dlc = 3;
    frame.data[0] = 0;   // Throttle
    frame.data[1] = 0;  // Brake
    frame.data[2] = -30; // Steering
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}

// Fonction pour tourner à gauche
void turn_left(int s) {
    struct can_frame frame;
    frame.can_id = 0x321;
    frame.can_dlc = 3;
    frame.data[0] = 0;   // Throttle
    frame.data[1] = 0;  // Brake
    frame.data[2] = 30; // Steering
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}
void left_start(int s) {
    struct can_frame frame;
    frame.can_id = 0x321;
    frame.can_dlc = 3;
    frame.data[0] = 30;   // Throttle
    frame.data[1] = 0;  // Brake
    frame.data[2] = 100; // Steering
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}

int main() {
    int s;
    int data[6];
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame;
    char fleche[3];
    int indice=0;

    // Create socket
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Socket");
        return 1;
    }

    // Get interface index
    strcpy(ifr.ifr_name, "vcan0");
    ioctl(s, SIOCGIFINDEX, &ifr);

    // Initialize address structure
    memset(&addr, 0, sizeof(addr));
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    // Bind socket to the specified interface
    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("Bind");
        return 1;
    }

    accelerate(s);
    sleep(5);     // Attendre que la voiture avance
    left_start(s);
    sleep(1);     // Attendre que la voiture tourne à droite
       // Attendre que la voiture avance
    while (1) {
        if (read(s, &frame, sizeof(struct can_frame)) < 0) {
            perror("Read");
            return 1;
        }
        if (frame.can_id == (0xC07|CAN_EFF_FLAG)) { // Trame de vitesse du véhicule
            int vehicle_speed = frame.data[0];
            if (vehicle_speed >= 50){
                brake(s);
            }
        }

        if (frame.can_id >= (0xC00|CAN_EFF_FLAG) && frame.can_id <= (0xC05|CAN_EFF_FLAG)) {
            
            data[frame.can_id - (0xC00|CAN_EFF_FLAG)] = (int)frame.data[0];

            int magie = (data[0]+data[1]+data[2]-data[3]-data[4]-data[5]);
            indice++;
            if (indice%6==0){
                if (magie>5){
                    turn_left(s);
                } else if (magie<-5){
                    turn_right(s);
                }
            }
        }
        // Attendre un court instant avant de lire la prochaine trame
        usleep(10000); // Attendre pendant 10 millisecondes (ajuster si nécessaire)
    }

    // Fermer la socket
    if (close(s) < 0) {
        perror("Close");
        return 1;
    }

    return 0;
}
