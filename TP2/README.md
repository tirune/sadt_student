# TP2

## Frames with semantic segmentation

![Couleur Route](image/1.png)

On retrouve sur cette image : 

road : rouge  
yield : bleu  
crossing : vert  
car_park : jaune  

## Describe what is seen by the vehicle

In the full_left area (ID 0xC00), the vehicle sees :  
the road : 0x5C  
yield marking : 0x0A 

In the left area (ID 0xC01), the vehicle sees :  
the road : 0x64  
yield marking : 0x01  
road crossing : 0x02  

In the middle_left area (ID 0xC02), the vehicle sees :  
the road : 0x64  
road crossing : 0x03  

In the middle_right area (ID 0xC03), the vehicle sees :  
the road : 0x64  
road crossing : 0x01  

In the right area (ID 0xC04), the vehicle sees :  
the road : 0x44  

In the full_right area (ID 0xC05), the vehicle sees :  
the road : 0x2B  


## vehicle_checker_student.c

Pour réaliser le vehicle_checker_student.c on réutilise le code de cantransmit auquel on ajoute les fonctions suivantes : 
 - resetlight(éteind les phares et clignotant)
 - blink_right(clignotant droit)
 - blink_left(clignotant gauche)
 - low_beam(petit phare)
 - high_beam(plein phare)

 - accelerate(accélerer)
 - brake(freiner)
 - turn_right(tourner à droite)
 - turn_left(tourner à gauche)

```python
void high_beam(int s) {
    struct can_frame frame;
    frame.can_id = 0x123;
    frame.can_dlc = 2;
    frame.data[0] = 0x02;//phare
    frame.data[1] = 0x00;//cligno
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}
```
Tout les codes gérant les lumiéres sont construit sur le même squelette frame.data[0] gére les phares (0 : éteint // 1 : petit // 2 : plein // 3  les deux ) et frame.data[1] gére les clignotant (0 : éteint // 1 : droite // 2 : gauche).

```python
void brake(int s) {
    struct can_frame frame;
    frame.can_id = 0x321;
    frame.can_dlc = 3;
    frame.data[0] = 0; // Throttle
    frame.data[1] = 100;  // Brake
    frame.data[2] = 0;  // Steering
    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        exit(EXIT_FAILURE);
    }
}
```
Tout les codes gérant les mouvements sont construit sur le même squelette frame.data[0] gére la pédale d'accélération (0 à 100), frame.data[0] gére la pédale de frein (0 à 100), frame.data[2] gére le volant(de -100, full droite à 100, full gauche).

Enfin on éxécute les fonctions afin de récopier les mouvements de vehicle_checker.

```python

//lumiéres
    blink_right(s);
    sleep(3);
    resetlight(s);
    blink_left(s);
    sleep(3);
    resetlight(s);

    low_beam(s);
    sleep(3);
    resetlight(s);
    high_beam(s);
    sleep(3);
    resetlight(s);


    //mouvement
    accelerate(s);
    sleep(5);     // Attendre que la voiture avance
    turn_left(s);
    sleep(1);     // Attendre que la voiture tourne à droite
    accelerate(s);
    sleep(6);
    brake(s);

```
![vehicle_checker_student](vidéo/1gif.gif)

## dashboard.c

Pour créer dashboard j'ai utiliser la base canreceive, puis on utilise une boucle while ou l'on va récolter les données toutes les 10 millisecondes (usleep(10000)).

```python
if (frame.can_id == (0xC06|CAN_EFF_FLAG)) { // Trame de vitesse du moteur
    int motor_speed = (frame.data[0] << 8) | frame.data[1];
    printf("Motor speed: %i rpm\n", motor_speed);
} else if (frame.can_id == (0xC07|CAN_EFF_FLAG)) { // Trame de vitesse du véhicule
    int vehicle_speed = frame.data[0];
    int gear_selection = frame.data[1];
    printf("Speed: %i km/h\n", vehicle_speed);
    printf("Gear: %i\n", gear_selection);
}
```

On regarde si les frame correspondent au frame qui contiennet nos données puis on affiche les valeurs qui nous intéressent.

```python
else if (frame.can_id >= (0xC00|CAN_EFF_FLAG) && frame.can_id <= (0xC05|CAN_EFF_FLAG)) {
    
    data[frame.can_id - (0xC00|CAN_EFF_FLAG)] = (int)frame.data[0];

    int magie = (data[0]+data[1]+data[2]-data[3]-data[4]-data[5]);
    indice++;
    if (indice%6==0){
        printf("Action to follow the road:  ");
        if (magie>5){
            printf("<-\n");
        } else if (magie<-5){
            printf("->\n");
        } else {
            printf("^\n");
        }
    }
}
```

Pour les frames 0xC00 à 0xC05 on va récuperer la frame[0] et l'insérer dans une variables data, on va ensuite utiliser la formule fourni pour savoir si la majorité de route se trouve à gauche ou à droite, pour savoir ou rouler.
Comme cette partie du code se repete 6 fois j'ai implémenter un compteur afin que le printf de 'Action to follow the road' ne s'execute qu'une fois tous les 6 coups, ce qui donne un résultat plus propre. Enfin on affiche la direction à suivre en fonction des données récolter

Voici le résultat Live.

![dashboard](vidéo/2gif.gif)

## road_follower.c

Concernant road_follower.c on utilise le même code que vehicle_checker_student.c sauf que cette fois ci le main on utilise la boucle while de dashboard.c et au lieu de printf la fléche gauche ou droite on tourne la voiture en conséquence.

J'ai aussi implémenter une limitation de vitesse : 
```python
if (frame.can_id == (0xC07|CAN_EFF_FLAG)) { // Trame de vitesse du véhicule
    int vehicle_speed = frame.data[0];
    if (vehicle_speed >= 50){
        brake(s);
    }
}
```
![](vidéo/3gif.gif)
