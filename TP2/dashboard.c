#include <stdio.h>
#include <unistd.h>
#include <string.h> // Include <string.h> for strcpy and memset
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>



int main() {
    int s;
    int data[6];
    struct sockaddr_can addr;
    struct ifreq ifr;
    struct can_frame frame; 
    char fleche[3];
    int indice=0;

    // Create socket
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Socket");
        return 1;
    }

    // Get interface index
    strcpy(ifr.ifr_name, "vcan0");
    ioctl(s, SIOCGIFINDEX, &ifr);

    // Initialize address structure
    memset(&addr, 0, sizeof(addr));
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    // Bind socket to the specified interface
    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("Bind");
        return 1;
    }

    while (1) {
        if (read(s, &frame, sizeof(struct can_frame)) < 0) {
            perror("Read");
            return 1;
        }

        if (frame.can_id == (0xC06|CAN_EFF_FLAG)) { // Trame de vitesse du moteur
            int motor_speed = (frame.data[0] << 8) | frame.data[1];
            printf("Motor speed: %i rpm\n", motor_speed);
        }
        else if (frame.can_id == (0xC07|CAN_EFF_FLAG)) { // Trame de vitesse du véhicule
            int vehicle_speed = frame.data[0];
            int gear_selection = frame.data[1];
            printf("Speed: %i km/h\n", vehicle_speed);
            printf("Gear: %i\n", gear_selection);
        } else if (frame.can_id >= (0xC00|CAN_EFF_FLAG) && frame.can_id <= (0xC05|CAN_EFF_FLAG)) {
            
            data[frame.can_id - (0xC00|CAN_EFF_FLAG)] = (int)frame.data[0];

            int magie = (data[0]+data[1]+data[2]-data[3]-data[4]-data[5]);
            indice++;
            if (indice%6==0){
                printf("Action to follow the road:  ");
                if (magie>5){
                    printf("<-\n");
                } else if (magie<-5){
                    printf("->\n");
                } else {
                    printf("^\n");
                }
            }
        }
        // Attendre un court instant avant de lire la prochaine trame
        usleep(10000); // Attendre pendant 10 millisecondes (ajuster si nécessaire)
    }


    // Close socket
    close(s);

    return 0;
}
