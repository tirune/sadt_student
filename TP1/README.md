# TP1

## Test combinations

![Test Envois](image/cansend1.png)
![Test Reception](image/candump1.png)

On test ici plusieurs combinaisons vie le cangen pour obtenir des resultats variés

## 42 in the 2nd octet

![42](image/42.png)

Pour mettre la valeur 42 dans le second octet on peut avoir deux méthodes, en utilisant un sprintf on va donner la valeur 42 en utilisant "B" qui vaut 42 en ASCII, ou bien on peut diretctement modifier la valeur en dur avec frame.data[1] (2nd octet on commence à 0).

## test.c

Pour réaliser le test.c on procéde en 3 étapes : 

###Filtre

```python
//Filre 
 
    struct can_filter rfilter[1];

    rfilter[0].can_id   = 0x100;
    rfilter[0].can_mask = 0x1FFFFF00;
    //rfilter[1].can_id   = 0x200;
    //rfilter[1].can_mask = 0x700;

    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));

    //Un filtre de réception est spécifié ici. Le programme ne lira que les messages dont l identifiant CAN correspond à can_id et dont le masque est can_mask.
```
On initie le filtre pour les ID entre 0x100 et 0x1FF.

###Envoi

```python
//Envoi

    frame.can_id = 0x8123;
    frame.can_dlc = 8;
    sprintf(frame.data, "ABO");

    //Un message CAN est préparé. can_id spécifie l identifiant du message et can_dlc spécifie la longueur des données.

    if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
        perror("Write");
        return 1

    //Le message est écrit sur le socket CAN à l aide de la fonction write.
    
```

On envoie une frame ayant l'ID 0x8123, et on modifie la data length à 8 bytes.

###Lecture

```python
//Lecture

    nbytes = read(s, &frame, sizeof(struct can_frame));

    if (nbytes < 0) {
        perror("Read");
        return 1;
    }
    //Le programme lit un message CAN du socket CAN. La fonction read retourne le nombre d octets lus.
    
    printf("0x%03X [%d] ",frame.can_id, frame.can_dlc);

    for (i = 0; i < frame.can_dlc; i++)
        printf("%02X ",frame.data[i]);

    printf("\r\n");

    //Le programme affiche l identifiant du message CAN et ses données.
```
Le programme vient lire les message envoyés sur vcan 0.

###Filtre candump

Pour filtrer directement avec le candump on peut utiliser candump vcan0 -i 100:1FF.

## Hack car

En inspectant simultanément le candump et en executant vehicle.checker on se rend compte quel frame correspond à quel frame. 
```python 

blink right blinkers
cansend vcan0 123#0001

blink left blinkers
cansend vcan0 123#0002

switch low beam on/off
cansend vcan0 123#0100
cansend vcan0 123#0000

switch high beam on/off
cansend vcan0 123#0200   
cansend vcan0 123#0000           
```
On remarque aussi que les commandes sont superposables : cansend vcan0 123#0201 activera et les clignotants droit et les pleins phares, de même un cansend vcan0 123#0003 activera les deux clignotants.
