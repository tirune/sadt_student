## MISRA Compliant

MISRA représente un référentiel méthodologique pour le développement de logiciels embarqués dans l'automobile. En suivant ses directives, on garantit la robustesse et la sécurité du code, réduisant ainsi les risques de défaillances. C'est essentiel pour assurer la fiabilité des véhicules.

### Required / Advisement

R 2.7 est un conseil

R 3.1 est requis pour être MISRA compliant

R 13.4 est un conseil

### Correction 

Voici ce que nous donne le code lorsqu'on execute un MISRA parser

![MISRA parser result 1](image/code pas propre.png)
![MISRA parser result 2](image/code pas propre 2.png)

Voici les erreurs rencontrés : 
 - 8.2 Lorsque nous définissons int main() nous rajoutons l'argument void pour montrer proprement que la fonction ne prend pas de paramétres.
 - 12.1 Nous rajoutons des paranthéses afin que les priorités opératoires soit appliqués.
 - 13.4 Nous devons définir proprement nos variables avant de les utiliser.
 - 15.5 L'usage de return 1 en fin de fonction n'est pas MISRA-compliant, il faut qu'il n'y est qu'un seul point de sortie à nos fonctions.
 - 15.7 Les constructions if /else doivent se finir par un else, rajout de else pour correspondre au normes MISRA.
 - 21.8 L'usage du exit de la librairie stdlib.h n'est pas MISRA-compliant.

![Voici le code corrigé](image/code propre.png)